package com.palmi;

import java.util.*;
import java.util.stream.Stream;

public class Grid {
    private Bubble[][] layout;
    private int minMoves;
    private int maxMoves;
    private Bubble bestBubble;

    public Grid(){
        Bubble[][] layout = new Bubble[6][5];
        for (int row=0; row<layout.length; row++) {
            for (int col=0; col<layout[row].length; col++) {
                layout[row][col] = new Bubble(col, row);
            }
        }

        this.setLayout(layout);
        this.calcGridPower();
        this.calcBestBubble();
    }

    // Costruttore clone.
    public Grid(Grid grid) {
        Bubble[][] layout = new Bubble[6][5];
        for (int row=0; row<layout.length; row++) {
            for (int col=0; col<layout[row].length; col++) {
                layout[row][col] = new Bubble(grid.getLayout()[row][col]);
            }
        }
        this.setLayout(layout);
        this.minMoves = grid.getMinMoves();
        this.maxMoves = grid.getMaxMoves();
        this.calcGridPower();
        this.calcBestBubble();
    }

    // Vengono lanciati il metodo per cambiare stato alla bolla selezionata e quello
    // per proseguire il gioco al turno successivo.
    // Se la bolla era in Status.EXPLODING, esploderà, chiamando il metodo this.explode
    public void play(Bubble pickedBubble) {
        if (pickedBubble.upgradeStatus()) { // Se lo stato cambia da EXPLODING a NONE, ritorna true.
            explode(pickedBubble);
        }
        Game.nextRound();
//        Bubble.Status bubbleStatus = pickedBubble.getStatus();
//        switch (bubbleStatus) {
//            case EXPLODING:
//                explode(pickedBubble);
//                break;
//            case NONE:
//                System.out.println("Non dovresti essere qui.");
//                break;
//            default:
//                pickedBubble.upgradeStatus();
//                break;
//        }
//        Game.nextRound();
    }

    // Viene chiesto al giocatore di inserire le coordinate [row] e [col]
    // in modo da ritornare la bolla di interesse.
    // Vengono eseguiti i controlli per evitare che vengano restituiti
    // InputMismatchException e IndexOutOfBoundsException.
    public Bubble pickBubble() {
        Scanner sc = new Scanner(System.in);
        int pickedRow = 10;
        int pickedCol = 10;
        do {
            try {
                System.out.println("* • ◕◕════SELEZIONA═UNA═BOLLA════◕◕ •*");
                System.out.print("Inserisci la riga [0-5]: ");
                pickedRow = sc.nextInt();
                if (pickedRow >= 0 && pickedRow <= 5) {
                    System.out.print("Inserisci la colonna [0-4]: ");
                    pickedCol = sc.nextInt();
                    if (pickedCol < 0 || pickedCol > 4) {
                        System.out.println("Inserisci una coppia di valori valida.\n");
                    } else if (layout[pickedRow][pickedCol].getStatus().getVALUE() == 0) {
                        System.out.println("Qui non c'è niente. Seleziona una bolla valida.\n");
                    }
                } else {
                    System.out.println("Inserisci una coppia di valori valida.\n");
                }
            } catch (InputMismatchException e) {
                System.out.println("Inserisci una coppia di valori valida.\n");
            }
            sc.nextLine();
        } while (pickedRow < 0 || pickedRow > 5 || pickedCol < 0 || pickedCol > 4
                || layout[pickedRow][pickedCol].getStatus().getVALUE() == 0);

        Game.subPlayerMoves();
        Game.playerLog("\n╔═════════╗\n" +
                "║ MOSSA " + (this.maxMoves - Game.getPlayerMoves()) + " ║\n" +
                "╚═════════╝\n" +
                "Hai inserito le coordinate " + pickedRow + ", " + pickedCol + "\n" +
                "e questo è il risultato.\n");
        return this.layout[pickedRow][pickedCol];
    }

    // Prende una bolla in ingresso e controlla la presenza di bolle vicine
    // reindirizzando al metodo upgradeClosestBubble, che ritorna true se trova
    // una bolla non vuota (!=Status.NONE).
    private void explode(Bubble pickedBubble) {
        int bubbleCol = pickedBubble.getCol();
        int bubbleRow = pickedBubble.getRow();

        // Controllo a destra
        for (int col=bubbleCol+1; col<this.layout[bubbleRow].length; col++) {
            if(upgradeClosestBubble(this.layout[bubbleRow][col])) break;
        }

        // Controllo a sinistra
        for (int col=bubbleCol-1; col>=0; col--) {
            if(upgradeClosestBubble(this.layout[bubbleRow][col])) break;
        }

        // Controllo in alto
        for (int row=bubbleRow-1; row>=0; row--) {
            if(upgradeClosestBubble(this.layout[row][bubbleCol])) break;
        }

        // Controllo in basso
        for (int row=bubbleRow+1; row<layout.length; row++) {
            if(upgradeClosestBubble(this.layout[row][bubbleCol])) break;
        }
    }

    // Viene chiamato da explode con la bolla più vicina a quella selezionata in ingresso.
    // Controlla se la bolla è gonfia (!=Status.NONE);
    // se lo è, chiama il metodo Bubble.upgradeStatus;
    // se la bolla era in Status.EXPLODING, viene chiamato di nuovo il metodo this.explode.
    private boolean upgradeClosestBubble (Bubble closestBubble) {
        if(closestBubble.getStatus() != Bubble.Status.NONE) {
            if(closestBubble.upgradeStatus()) {
                this.explode(closestBubble);
            }
            return true;
        }
        return false;
    }

    // Reindirizza tutte le bolle non esplose al metodo
    // per calcolare il power della bolla.
    public void calcGridPower(){
        Arrays.stream(layout)
                .flatMap(Stream::of)
                .forEach(bubble -> {
                    if (bubble.getStatus() == Bubble.Status.NONE) {
                        bubble.setPower(0);
                    } else {
                        this.calcPower(bubble);
                    }
                });
//        for (Bubble[] bubbles : this.layout) {
//            for (Bubble bubble : bubbles) {
//                if (bubble.getStatus().getVALUE() == 0) {
//                    bubble.setPower(0);
//                    continue;
//                }
//                this.calcPower(bubble);
//            }
//        }
    }

    // Calcola il power della bolla in base al valore delle bolle vicine.
    private void calcPower(Bubble bubble) {
        int col = bubble.getCol();
        int row = bubble.getRow();

        bubble.setPower(bubble.getStatus().getVALUE());

        // devo creare uno stream per filtrare le bolle vicine di interesse TODO

//        Arrays.stream(layout)
//                .flatMap(Stream::of)
//                .filter(bubble1 ->
//                        (bubble1.getRow() <= row+2 && bubble1.getRow() >= row-2 && bubble1.getCol() == col) || // Prendo bolle su stessa colonna
//                        (bubble1.getCol() <= col+2 && bubble1.getCol() >= col+2 && bubble1.getRow() == row) || // Prendo bolle su stessa riga
//                        (bubble1.getRow() == row+1 && bubble1.getCol() == col+1) ||
//                        (bubble1.getRow() == row+1 && bubble1.getCol() == col-1) ||
//                        (bubble1.getRow() == row-1 && bubble1.getCol() == col+1) ||
//                        (bubble1.getRow() == row-1 && bubble1.getCol() == col-1)
//                )
//                .filter();

//        List<Bubble> prova = Arrays.stream(layout)
//                .flatMap(Stream::of)
//                .filter(bubble1 -> (bubble1.getRow() == row+1 && bubble1.getCol() == col+1) ||
//                        (bubble1.getRow() == row+1 && bubble1.getCol() == col-1) ||
//                        (bubble1.getRow() == row-1 && bubble1.getCol() == col+1) ||
//                        (bubble1.getRow() == row-1 && bubble1.getCol() == col-1))
//                .filter(bubble1 -> bubble1.getStatus() != Bubble.Status.NONE)
//                .peek(bubble1 -> bubble.addPower(bubble1))
//                .collect(Collectors.toList());



        // Bubble Top
        if (row!=0) {
            Bubble bubbleTop = this.layout[row-1][col];
            if (bubbleTop.getStatus().getVALUE() != 0) {
                bubble.addPower(bubbleTop);
                if (col!=0) {
                    Bubble bubbleTopLeft = this.layout[row-1][col-1];
                    bubble.addPower(bubbleTopLeft);
                }
                if (col!=4) {
                    Bubble bubbleTopRight = this.layout[row-1][col+1];
                    bubble.addPower(bubbleTopRight);
                }
                if (row!=1) {
                    Bubble bubbleTopTop = this.layout[row-2][col];
                    bubble.addPower(bubbleTopTop);
                }
            }

        }

        // Bubble Bottom
        if (row!=5) {
            Bubble bubbleBottom = this.layout[row+1][col];
            if (bubbleBottom.getStatus().getVALUE() != 0) {
                bubble.addPower(bubbleBottom);
                if (col!=0) {
                    Bubble bubbleBottomLeft = this.layout[row+1][col-1];
                    bubble.addPower(bubbleBottomLeft);
                }
                if (col!=4) {
                    Bubble bubbleBottomRight = this.layout[row+1][col+1];
                    bubble.addPower(bubbleBottomRight);
                }
                if (row!=4) {
                    Bubble bubbleBottomBottom = this.layout[row+2][col];
                    bubble.addPower(bubbleBottomBottom);
                }
            }

        }

        // Bubble Left
        if (col!=0) {
            Bubble bubbleLeft = this.layout[row][col-1];
            if (bubbleLeft.getStatus().getVALUE() != 0) {
                bubble.addPower(bubbleLeft);
                if (col!=1) {
                    Bubble bubbleLeftLeft = this.layout[row][col-2];
                    bubble.addPower(bubbleLeftLeft);
                }
            }

        }

        // Bubble Right
        if (col!=4) {
            Bubble bubbleRight = this.layout[row][col+1];
            if (bubbleRight.getStatus().getVALUE() != 0) {
                bubble.addPower(bubbleRight);
                if (col!=3) {
                    Bubble bubbleRightRight = this.layout[row][col+2];
                    bubble.addPower(bubbleRightRight);
                }
            }

        }
    }

    // Compara il power di tutte le bolle e setta la bolla con power maggiore
    // come bestBubble della Grid tramite setBestBubble.
    public void calcBestBubble() {
        this.setBestBubble(
                Arrays.stream(layout)
                        .flatMap(Stream::of)
                        .max(Comparator.comparing(Bubble::getPower))
                        .orElse(null)
        );
//        int maxPower = 0;
//        for (Bubble[] bubbles : this.layout) {
//            for (Bubble bubble : bubbles) {
//                maxPower = Math.max(bubble.getPower(), maxPower);
//            }
//        }
//
//        for (Bubble[] bubbles : this.layout) {
//            for (Bubble bubble : bubbles) {
//                if (bubble.getPower() == maxPower) {
//                    this.setBestBubble(bubble);
//                    return;
//                }
//            }
//        }
    }

    // Stampa la bolla con più power in chiaro se il giocatore
    // ha deciso di attivare i consigli.
    public void printHints(boolean cheatIsOn){
        if (cheatIsOn) {
            System.out.println("\n═.✵.═CONSIGLI══.✵.═");
            System.out.println("La bolla più forte ha power: " + this.bestBubble.getPower());
            System.out.println("La bolla più forte ha coordinate "+this.bestBubble.getRow()+", "+this.bestBubble.getCol());
        }
    }

    // Stampa la griglia.
    public void printLayout() {
        String colIndex = "\n  ║ 0 ║ 1 ║ 2 ║ 3 ║ 4 ║";
        System.out.println(colIndex);
        Game.playerLog(colIndex + "\n");

        Arrays.stream(layout)
                .forEach(bubbles -> {
                    String line = "════════════════════════╣";
                    System.out.println(line);
                    Game.playerLog(line + "\n");
                    Arrays.stream(bubbles)
                            .forEach(bubble -> {
                                char bubbleSymbol = bubble.getStatus().getSYMBOL();
                                if (bubble.getCol()==0) {
                                    String rowIndex = bubble.getRow() + " ║ ";
                                    System.out.print(rowIndex);
                                    Game.playerLog(rowIndex);
                                }
                                String bubbleInGrid = bubbleSymbol + " ║ ";
                                System.out.print(bubbleInGrid);
                                Game.playerLog(bubbleInGrid);
                                if (bubble.getCol()==4) {
                                    System.out.println();
                                    Game.playerLog("\n");
                                }
                            });
                });

//        for (int row=0; row<this.layout.length; row++) {
//            String line = "════════════════════════╣";
//            System.out.println(line);
//            Game.playerLog(line + "\n");
//            for (int col=0; col<this.layout[row].length; col++) {
//                char bubble = this.layout[row][col].getStatus().getSYMBOL();
//
//                if (col==0) {
//                    String rowIndex = row + " ║ ";
//                    System.out.print(rowIndex);
//                    Game.playerLog(rowIndex);
//                }
//                String bubbleInGrid = bubble + " ║ ";
//                System.out.print(bubbleInGrid);
//                Game.playerLog(bubbleInGrid);
//                if (col==4) {
//                    System.out.println();
//                    Game.playerLog("\n");
//                }
//            }
//        }
    }

    // Sommo i valori delle bolle tra di loro e se la somma totale è 0, tutte le bolle
    // sono esplose e la condizione di vittoria si avvera.
    public boolean isWin(){
//        int sum = 0;
        int sum = Arrays.stream(layout)
                .mapToInt(bubbles -> Arrays.stream(bubbles)
                        .mapToInt(bubble -> bubble.getStatus().getVALUE())
                        .sum())
                .sum();
//        for (int row=0; row < getLayout().length; row++) {
//            for (int col=0; col < getLayout()[row].length; col++) {
//                sum += getLayout()[row][col].getStatus().getVALUE();
//            }
//        }
        return sum==0;
    }

    public Bubble[][] getLayout() {
        return layout;
    }
    public void setLayout(Bubble[][] layout) {
        this.layout = layout;
    }
    public int getMinMoves() {
        return minMoves;
    }
    public void setMinMoves(int minMoves) {
        this.minMoves = minMoves;
    }
    public Bubble getBestBubble() {
        return bestBubble;
    }
    public void setBestBubble(Bubble bestBubble) {
        this.bestBubble = bestBubble;
    }
    public int getMaxMoves() {
        return maxMoves;
    }
    public void setMaxMoves(int maxMoves) {
        this.maxMoves = maxMoves;
    }
}
