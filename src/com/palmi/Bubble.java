package com.palmi;

import java.util.Random;

public class Bubble {
    private final int col;
    private final int row;
    private int power;
    private Status status;

    public Bubble(int col, int row){
        this.col = col;
        this.row = row;
        this.status = Status.randomStatus();
        this.power = this.status.getVALUE();
    }

    // Viene cambiato lo stato della bolla. Se esplode, ritorna true.
    public boolean upgradeStatus() {
        switch (status) {
            case EMPTY:
                setStatus(Status.BLOWN);
                break;
            case BLOWN:
                setStatus(Status.EXPLODING);
                break;
            case EXPLODING:
                setStatus(Status.NONE);
                return true;
        }
        return false;
    }

    // Viene chiamato dal metodo Grid.calcPower per calcolare il power della bolla
    // prendendo il valore della bolla vicina e sommandolo al proprio power.
    public void addPower(Bubble closestBubble) {
        this.power += closestBubble.getStatus().getVALUE();
    }

    // Costruttore clone.
    public Bubble(Bubble bubble) {
        this.col = bubble.getCol();
        this.row = bubble.getRow();
        this.power = bubble.getPower();
        this.status = bubble.getStatus();
    }

    public int getCol() {
        return col;
    }
    public int getRow() {
        return row;
    }
    public Status getStatus() {
        return status;
    }
    public int getPower() {
        return power;
    }
    public void setPower(int power) {
        this.power = power;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status {
        NONE("none", 0, '○'),
        EMPTY("empty", 1, '⦿'),
        BLOWN("blown", 2, '⬤'),
        EXPLODING("exploding", 3, '✪');

        private final String STATUS;
        private final Integer VALUE;
        private final Character SYMBOL;

        Status(String status, int value, char symbol) {
            this.STATUS = status;
            this.VALUE = value;
            this.SYMBOL = symbol;
        }

        public String getSTATUS() {
            return STATUS;
        }

        public Integer getVALUE() {
            return VALUE;
        }

        public Character getSYMBOL() {
            return SYMBOL;
        }

        public static Status randomStatus() {
            Status[] values = Status.values();
            int length = values.length;
            int randIndex = new Random().nextInt(length);
            return values[randIndex];
        }
    }

    @Override
    public String toString() {
        return "Bubble{" +
                "col=" + col +
                ", row=" + row +
                ", power=" + power +
                ", status=" + status +
                '}';
    }
}
