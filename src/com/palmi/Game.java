package com.palmi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Game {
    private static Grid grid;
    private static Grid simulatedGrid;
    private static boolean isSimulation;
    private static int playerMoves;
    private static boolean cheatIsOn;
    private static StringBuilder playerLog = new StringBuilder();

    private static final File PLAYERLOGFILE = new File("player_log.txt");
    private static FileWriter fw;

    static {
        try {
            fw = new FileWriter(PLAYERLOGFILE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final PrintWriter PW = new PrintWriter(fw);

    // Viene lanciato come primo metodo per inizializzare la partita.
    // Viene simulata una partita utilizzando le bolle migliori.
    // per generare il numero minimo e massimo di mosse a disposizione.
    // Chiede al giocatore se vuole attivare i consigli e se vuole
    // consultare le regole.
    public static void start(){
        playerLog.setLength(0);
        Game.grid = new Grid();
        Game.setIsSimulation(true);
        Game.simulatedGrid = new Grid(grid);
        Game.simulatedGrid.play(Game.simulatedGrid.getBestBubble());
        System.out.println("\nVuoi attivare i consigli? Sì [Y] / No [Qualunque tasto]");
        if (new Scanner(System.in).nextLine().equalsIgnoreCase("y")) {
            Game.cheatIsOn = true;
            System.out.println("\n╔═══════════════════╗\n" +
                    "║ CONSIGLI ATTIVATI ║\n" +
                    "╚═══════════════════╝\n");
        } else {
            Game.cheatIsOn = false;
            System.out.println("\n╔══════════════════════╗\n" +
                    "║ CONSIGLI DISATTIVATI ║\n" +
                    "╚══════════════════════╝\n");

        }
        System.out.println("Vuoi consultare le regole? Sì [Y] / No [Qualunque tasto]");
        if (new Scanner(System.in).nextLine().equalsIgnoreCase("y")) {
            System.out.println("\n  _____                  _      \n" +
                    " |  __ \\                | |     \n" +
                    " | |__) |___  __ _  ___ | | ___ \n" +
                    " |  _  // _ \\/ _` |/ _ \\| |/ _ \\\n" +
                    " | | \\ \\  __/ (_| | (_) | |  __/\n" +
                    " |_|  \\_\\___|\\__, |\\___/|_|\\___|\n" +
                    "              __/ |             \n" +
                    "             |___/              \n\n" +
                    "Seleziona una bolla dalla griglia. A ogni selezione, la bolla\n" +
                    "verrà gonfiata e passerà allo stato successivo:\n" +
                    "SGONFIA       -> GONFIA A METÀ\n" +
                    "GONFIA A METÀ -> GONFIA\n" +
                    "GONFIA        -> ESPLOSA (VUOTO)\n" +
                    "\n" +
                    "Selezionando una bolla in procinto di esplodere, questa esploderà̀\n" +
                    "e scomparirà dalla griglia. Inoltre, propagherà l’esplosione nelle\n" +
                    "direzioni verticali e orizzontali.\n" +
                    "\n" +
                    "Quando una propagazione raggiunge una bolla, essa si comporta come\n" +
                    "se fosse stata selezionata e si gonfia passando allo stato\n" +
                    "successivo. Se la bolla è già nello stato in procinto di esplodere,\n" +
                    "esploderà̀ propagando l’esplosione a sua volta.\n" +
                    "\n" +
                    "Il gioco termina quando non ci sono più bolle sulla griglia.\n" +
                    "Potrai vincere se riuscirai a far scoppiare tutte le bolle\n" +
                    "nel numero di mosse prestabilito.\n" +
                    "\n" +
                    "╔═════════╗\n" +
                    "║ LEGENDA ║\n" +
                    "╚═════════╝\n" +
                    "○ = Nessuna bolla       | VALORE = 0\n" +
                    "⦿ = Bolla vuota         | VALORE = 1\n" +
                    "⬤ = Bolla gonfia a metà | VALORE = 2\n" +
                    "✪ = Bolla gonfia        | VALORE = 3");
        }
        Game.playerLog("╔══════════════════╗\n" +
                "║ GRIGLIA INIZIALE ║\n" +
                "╚══════════════════╝\n");
        Game.grid.printLayout();
        Game.grid.printHints(cheatIsOn);
        Game.playerMoves = Game.grid.getMaxMoves();
        Game.movesLeft();
        Game.playerLog("\nIl minimo di mosse da fare è: " + Game.grid.getMinMoves() +
                "\nHai " + Game.grid.getMaxMoves() + " mosse a disposizione.\n");
        Game.grid.play(Game.grid.pickBubble());

    }

    // Viene chiamato da Grid.play() quando la bolla scelta ha finito di
    // di esplodere (se EXPLODING) o quando qualunque altra bolla
    // ha cambiato stato, in modo da continuare la partita.
    // Se tutte le bolle sono esplose, il metodo Grid.isWin()
    // restituisce true e si avvera la condizione di vittoria.
    // Se il giocatore non ha più mosse a disposizione,
    // si avvera la condizione di sconfitta.
    public static void nextRound(){
        if (Game.isSimulation) {
            if (Game.simulatedGrid.isWin()) {
                Game.grid.setMinMoves(Game.grid.getMinMoves()+1);
                Game.grid.setMaxMoves(Game.grid.getMinMoves()+3);
                Game.setIsSimulation(false);
            } else {
                Game.grid.setMinMoves(Game.grid.getMinMoves()+1);
                Game.grid.setMaxMoves(Game.grid.getMinMoves()+3);
                Game.simulatedGrid.calcGridPower();
                Game.simulatedGrid.calcBestBubble();
                Game.simulatedGrid.play(Game.simulatedGrid.getBestBubble());
            }
        } else {
            if (Game.grid.isWin()) {
                Game.grid.printLayout();
                int nMoves = Game.grid.getMaxMoves() - Game.playerMoves;
                String moves = nMoves == 1 ? "mossa" : "mosse";
                String victory = "\n»»————-　★　————-««\n" +
                        "   COMPLIMENTI!\n" +
                        "    HAI VINTO!\n" +
                        "»»————-　★　————-««\n" +
                        "\n" +
                        "Hai vinto in " + nMoves + " " + moves + ".";
                Game.playerLog(victory + "\n\n");
                System.out.println(victory);
            } else if (playerMoves==0) {
                Game.grid.printLayout();
                String defeat = "\nMi dispiace. Hai perso.";
                Game.playerLog(defeat + "\n\n");
                System.out.println(defeat);
            } else {
                Game.grid.calcGridPower();
                Game.grid.calcBestBubble();
                Game.grid.printLayout();
                Game.grid.printHints(cheatIsOn);
                Game.movesLeft();
                Game.grid.play(Game.grid.pickBubble());
            }
        }

    }

    // Diminuisce le mosse a disposizione del giocatore.
    public static void subPlayerMoves() {
        Game.playerMoves -= 1;
    }

    // Stampa il minimo di mosse da fare e le mosse rimanenti.
    public static void movesLeft() {
        System.out.println("\nIl minimo di mosse da fare è: " + Game.grid.getMinMoves());
        System.out.println("Hai a disposizione " + Game.playerMoves + "/" + Game.grid.getMaxMoves() + " mosse.\n");
    }

    // Aggiunge stringhe allo StringBuilder playerLog, che verrà poi
    // utilizzato dal PrintWriter per compilare il log di gioco.
    public static void playerLog(String text) {
        playerLog.append(text);
    }

    // Scrive sul playerLog.txt ciò che è stato popolato nello StringBuilder
    // ma rimane aperto nel caso in cui il giocatore voglia giocare altre partite.
    public static void createPlayerLog(String text, int gameNr) {
        Game.PW.println(" __| |_______________________________| |__\n" +
                "(__   _______________________________   __)\n" +
                "   | |                               | |\n" +
                "   | |           PARTITA "+ gameNr +"           | |\n" +
                " __| |_______________________________| |__\n" +
                "(__   _______________________________   __)\n" +
                "   | |                               | |\n");
        Game.PW.write(text);
    }

    // Chiama la chiusura del PrintWriter.
    public static void closePlayerLog() {
        Game.PW.close();
    }

    public static void setIsSimulation(boolean isSimulation) {
        Game.isSimulation = isSimulation;
    }
    public static int getPlayerMoves() {
        return playerMoves;
    }
    public static StringBuilder getPlayerLog() {
        return playerLog;
    }
}
